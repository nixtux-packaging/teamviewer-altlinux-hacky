Summary: TeamViewer ALT Linux Hacky
Name: teamviewer-altlinux-hacky
Version: 0.1
Release: alt1
License: Proprietary
Group: System/Configuration/Packaging
Url: https://gitlab.com/nixtux-packaging/teamviewer-altlinux-hacky
BuildArch: noarch
Packager: Mikhail Novosyolov <mikhailnov@dumalogiya.ru>
Source0: teamviewer_11.0.93329.i686.rpm

AutoReq: no

%description -l ru_RU.UTF-8
Упаковываем официальный RPM пакет TeamViewer 11
(именно старый 11) как файл в "фейковый" RPM-пакет,
чтобы потом командой
rpm -i --nodeps /usr/share/teamviewer-altlinux-hacky/teamviewer11.rpm
его установить и удалить фейк-пакет
apt-get remove teamviewer-altlinux-hacky

%prep
#%setup

%install
mkdir -p %buildroot%_datadir/teamviewer-altlinux-hacky/
install -p -m644 %{SOURCE0} %buildroot%_datadir/teamviewer-altlinux-hacky/teamviewer11.rpm

%files
%_datadir/teamviewer-altlinux-hacky/teamviewer11.rpm

%changelog
* Sat Jul 28 2018 Mikhail Novosyolov <mikhailnov@dumalogiya.ru> 0.1-alt1
- Initial packaging

############################################################################
# https://www.teamviewer.com/ru/download/previous-versions/
# http://download.teamviewer.com/download/version_11x/teamviewer.i686.rpm
# RPM скачать и положить в корень этой папки

# для сборки из корня этой папки запустить rpmbb (etersoft-build-utils)
############################################################################
